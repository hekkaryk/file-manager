﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace PPFileManager_ConsoleApp
{
    public static class FileManager
    {
        private static DirectoryInfo[] GetFolders(string path, int level)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                DirectoryInfo[] dirs = dir.GetDirectories();
                List<DirectoryInfo> allFolders = new List<DirectoryInfo>();
                allFolders.AddRange(AddFolders(dirs, level));
                foreach (DirectoryInfo subdir in dirs)
                {
                    allFolders.AddRange(GetFolders(subdir.FullName, level));
                }
                DirectoryInfo[] outputFolders = new DirectoryInfo[allFolders.Count];
                for (int i = 0; i < allFolders.Count; i++)
                {
                    outputFolders[i] = allFolders[i];
                }
                return outputFolders;
            }
            catch
            {
                if (level > 0)
                {
                    ConsoleColor prev = Console.BackgroundColor;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("! FAIL: GetFiles.");
                    Console.BackgroundColor = prev;
                }
                return null;
            }
        }

        private static List<DirectoryInfo> AddFolders(DirectoryInfo[] folders, int level)
        {
            try
            {
                List<DirectoryInfo> output = new List<DirectoryInfo>();
                foreach (DirectoryInfo folder in folders)
                {
                    output.Add(folder);
                }
                return output;
            }
            catch
            {
                if (level > 0)
                {
                    ConsoleColor prev = Console.BackgroundColor;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("FAIL: AddFolders.");
                    Console.BackgroundColor = prev;
                }
                return null;
            }
        }

        private static FileInfo[] GetFiles(string path, int level)
        {
            try
            {
                DirectoryInfo dir = new DirectoryInfo(path);
                DirectoryInfo[] dirs = dir.GetDirectories();
                FileInfo[] files = dir.GetFiles();
                List<FileInfo> allFiles = new List<FileInfo>();
                allFiles.AddRange(AddFiles(files, level));
                foreach (DirectoryInfo subdir in dirs)
                {
                    allFiles.AddRange(GetFiles(subdir.FullName, level));
                }
                FileInfo[] outputFiles = new FileInfo[allFiles.Count];
                for (int i = 0; i < allFiles.Count; i++)
                {
                    outputFiles[i] = allFiles[i];
                }
                return outputFiles;
            }
            catch
            {
                if (level > 0)
                {
                    ConsoleColor prev = Console.BackgroundColor;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("! FAIL: GetFiles.");
                    Console.BackgroundColor = prev;
                }
                return null;
            }
        }

        private static List<FileInfo> AddFiles(FileInfo[] files, int level)
        {
            try
            {
                List<FileInfo> fileInfos = new List<FileInfo>();
                foreach (FileInfo file in files)
                {
                    fileInfos.Add(file);
                }
                return fileInfos;
            }
            catch
            {
                if (level > 0)
                {
                    ConsoleColor prev = Console.BackgroundColor;
                    Console.BackgroundColor = ConsoleColor.Red;
                    Console.WriteLine("FAIL: AddFiles.");
                    Console.BackgroundColor = prev;
                }
                return null;
            }
        }

        public static void RemoveDuplicates(int level, string input = "input", string output = "output")
        {
            Console.WriteLine($"Logging level {level}");
            try
            {
                string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + Path.DirectorySeparatorChar;

                FileInfo[] filesFileinfo = GetFiles(path + input, level);
                string message = level == 3 ? "\nList of all processed files:\n" : "";

                List<FileData> filesFileData = new List<FileData>();
                foreach(FileInfo info in filesFileinfo)
                {
                    filesFileData.Add(new FileData(info));
                }

                List<FileData> singles = new List<FileData>();

                foreach (FileData file in filesFileData)
                {
                    try
                    {
                        if (!singles.Exists(m => m.Equals(file)))
                        {
                            singles.Add(file);
                            string newFileName = file.FileInfo.FullName.Replace(path + input, path + output);
                            Directory.CreateDirectory(newFileName.Replace(file.FileInfo.Name, ""));
                            File.Copy(file.FileInfo.FullName, newFileName, true);
                            string _message = "";
                            if (level == 3)
                            {
                                _message = "Copied: " + file.FileInfo.FullName + "\n";
                            }
                            else if (level == 2)
                            {
                                _message = "+";
                            }
                            if (level > 1)
                            {
                                Console.Write(_message);
                            }
                        }
                        else
                        {
                            string _message = "";
                            if (level == 3)
                            {
                                _message = "Ignored: " + file.FileInfo.FullName + "\n";
                            }
                            else if (level == 2)
                            {
                                _message = ".";
                            }
                            if (level > 1)
                            {
                                Console.Write(_message);
                            }
                        }
                        if (level == 3)
                        {
                            message += file.FileInfo.FullName + "\n";
                        }
                    }
                    catch (Exception e)
                    {
                        if (level > 0)
                        {
                            ConsoleColor prev = Console.BackgroundColor;
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine($"! Failed to process: {file.FileInfo.FullName}");
                            Console.WriteLine("\t" + e.Message);
                            Console.BackgroundColor = prev;
                        }
                    }
                }
                if (level > 0)
                {
                    Console.Write(message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                Environment.Exit(-1);
            }
        }

        public static void MoveLargeFiles(int level, int size = 1024 * 1024 * 50, string input = "input", string output = "output")
        {
            Console.WriteLine($"Logging level {level}");
            try
            {
                string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + Path.DirectorySeparatorChar + input;

                FileInfo[] files = GetFiles(path, level);
                string message = level == 3 ? "\nList of all processed files:\n" : "";

                foreach (FileInfo file in files)
                {
                    try
                    {
                        if (file.Length >= size)
                        {
                            string newFileName = file.FullName.Replace(input, output);
                            Directory.CreateDirectory(newFileName.Replace(file.Name, ""));
                            File.Move(file.FullName, newFileName);
                            string _message = "";
                            if (level == 3)
                            {
                                _message = "Moved: " + file.FullName + "\n";
                            }
                            else if (level == 2)
                            {
                                _message = "-";
                            }
                            if (level > 1)
                            {
                                Console.Write(_message);
                            }
                        }
                        else
                        {
                            string _message = "";
                            if (level == 3)
                            {
                                _message = "Ignored: " + file.FullName + "\n";
                            }
                            else if (level == 2)
                            {
                                _message = ".";
                            }
                            if (level > 1)
                            {
                                Console.Write(_message);
                            }
                        }
                        if (level == 3)
                        {
                            message += file.FullName + "\n";
                        }
                    }
                    catch (Exception e)
                    {
                        if (level > 0)
                        {
                            ConsoleColor prev = Console.BackgroundColor;
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine($"! Failed to process: {file.FullName}");
                            Console.WriteLine("\t" + e.Message);
                            Console.BackgroundColor = prev;
                        }
                    }
                }
                if (level > 0)
                {
                    Console.Write(message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                Environment.Exit(-1);
            }
        }

        public static void RemoveEmptyFolders(int level, string input = "input")
        {
            Console.WriteLine($"Logging level {level}");
            try
            {
                string path = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + Path.DirectorySeparatorChar + input;

                DirectoryInfo[] folders = GetFolders(path, level);
                string message = level == 3 ? "\nList of all processed files:\n" : "";

                foreach (DirectoryInfo folder in folders)
                {
                    try
                    {
                        if (folder.GetDirectories().Length == 0 && folder.GetFiles().Length == 0)
                        {
                            Directory.Delete(folder.FullName, false);
                            string _message = "";
                            if (level == 3)
                            {
                                _message = "Removed directory: " + folder.FullName + "\n";
                            }
                            else if (level == 2)
                            {
                                _message = "-";
                            }
                            if (level > 1)
                            {
                                Console.Write(_message);
                            }
                        }
                        else
                        {
                            string _message = "";
                            if (level == 3)
                            {
                                _message = "Ignored: " + folder.FullName + "\n";
                            }
                            else if (level == 2)
                            {
                                _message = ".";
                            }
                            if (level > 1)
                            {
                                Console.Write(_message);
                            }
                        }
                        if (level == 3)
                        {
                            message += folder.FullName + "\n";
                        }
                    }
                    catch (Exception e)
                    {
                        if (level > 0)
                        {
                            ConsoleColor prev = Console.BackgroundColor;
                            Console.BackgroundColor = ConsoleColor.Red;
                            Console.WriteLine($"! Failed to process: {folder.FullName}");
                            Console.WriteLine("\t" + e.Message);
                            Console.BackgroundColor = prev;
                        }
                    }
                }
                if (level > 0)
                {
                    Console.Write(message);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadKey();
                Environment.Exit(-1);
            }
        }
    }
}
