﻿using System;
using System.IO;
using System.Security.Cryptography;

namespace PPFileManager_ConsoleApp
{
    public static class Checksum
    {
        public static string CalculateSHA256(string path)
        {
            using (FileStream stream = File.OpenRead(path))
            {
                SHA256CryptoServiceProvider sha = new SHA256CryptoServiceProvider();
                byte[] hash = sha.ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
        }
    }
}
