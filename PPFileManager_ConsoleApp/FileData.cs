﻿using System.IO;

namespace PPFileManager_ConsoleApp
{
    public class FileData
    {
        public FileInfo FileInfo { get; set; }
        public string SHA256 { get; set; }

        public FileData(FileInfo FileInfo)
        {
            this.FileInfo = FileInfo;
            SHA256 = Checksum.CalculateSHA256(this.FileInfo.FullName);
        }

        public bool Equals(FileData fileData)
        {
            if (SHA256 == fileData.SHA256 && FileInfo.Name == fileData.FileInfo.Name && FileInfo.LastWriteTimeUtc == fileData.FileInfo.LastWriteTimeUtc) return true;
            return false;
        }
    }
}
