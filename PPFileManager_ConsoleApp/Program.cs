﻿using System;
using System.IO;
using System.Reflection;

namespace PPFileManager_ConsoleApp
{
    class Program
    {

        static void Main(string[] args)
        {
            ConsoleKey[] validOptions = { ConsoleKey.D1, ConsoleKey.D2, ConsoleKey.D3, ConsoleKey.Q };
            while (true)
            {
                Console.WriteLine("[1] File duplicates remover");
                Console.WriteLine("[2] Large file mover");
                Console.WriteLine("[3] Empty folder remover");
                Console.WriteLine("[q] Quit");

                ConsoleKeyInfo cki = Console.ReadKey();
                if (cki.Key == ConsoleKey.Q)
                {
                    Console.WriteLine("\nProgram has finished working.\nPress any key to exit...");
                    Console.ReadKey();
                    Environment.Exit(0);
                }
                if (!Array.Exists(validOptions, m => m == cki.Key))
                {
                    Console.WriteLine($"There is no option assigned to {cki.KeyChar}.");
                }
                else
                {
                    Console.WriteLine("\nInput folder name: ");
                    string input = Console.ReadLine();
                    string output = "";
                    if (cki.Key != ConsoleKey.D3)
                    {
                        Console.WriteLine("Output folder name: ");
                        output = Console.ReadLine();
                    }
                    Console.WriteLine("Select log level (high log level slow down process):");
                    Console.WriteLine("[0] No output");
                    Console.WriteLine("[1] Only excpetions");
                    Console.WriteLine("[2] + copied . ignored");
                    Console.WriteLine("[3] EVERYTHING");
                    int level = Convert.ToInt16(Console.ReadKey().KeyChar - 48);
                    Console.WriteLine();
                    if (input == "" && output == "")
                    {
                        Console.WriteLine("Defaulting input as \"input\" and output as \"input\".");
                        input = "input";
                        output = "output";
                    }
                    else if (input == "" && output != "")
                    {
                        Console.WriteLine("Defaulting input as \"input\".");
                        input = "input";
                    }
                    else if (input != "" && output == "")
                    {
                        Console.WriteLine("Defaulting output as \"input\".");
                        output = "output";
                    }
                    if (cki.Key == ConsoleKey.D1)
                    {
                        Deduplicate(level, input, output);
                    }
                    else if (cki.Key == ConsoleKey.D2)
                    {
                        MoveLargeFiles(level, input, output);
                    }
                    else if (cki.Key == ConsoleKey.D3)
                    {
                        RemoveEmptyFolders(level, input, output);
                    }
                }
            }
        }

        private static void RemoveEmptyFolders(int level = 3, string input = "input", string output = "output")
        {
            FileManager.RemoveEmptyFolders(level, input);
        }

        private static void MoveLargeFiles(int level = 3, string input = "input", string output = "output")
        {
            Console.WriteLine("\nHow big file have to be to be moved? [in MB]");
            int size = 1024 * 1024 * Convert.ToInt32(Console.ReadLine());
            FileManager.MoveLargeFiles(level, size, input, output);
        }

        private static void Deduplicate(int level = 3, string input = "input", string output = "output")
        {
            Console.WriteLine("\nExecuting deduplicator at:" + Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) + Path.DirectorySeparatorChar);
            FileManager.RemoveDuplicates(level, input, output);
        }
    }
}
